# aunit #
aunit - Arduino unit test helper functions

A set of functions used to facilitate unit testing on an Arduino.

Designed to be lightweight due to the limited resources of some
Arduino platforms.  This is not a complete unit test framework.

### Documentation ###
Documentation is contained in the aunit.h header file.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License
https://opensource.org/licenses/MIT
