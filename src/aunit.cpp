/******************************************************************************
 * aunit.c
 * Copyright (c) 2015 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * Arduino unit test helper functions.
 *
 * A set of functions used to facilitate unit testing on an Arduino.
 *
 * Designed to be lightweight due to the limited resources of some
 * Arduino platforms.  This is not a complete unit test framework.
 *
 * Function names are all caps, as this appears to be the tradition with
 * unit test frameworks.  It also makes the test functions more noticeable
 * in the code.
 */
 
/******************************************************************************
 * Lint options.
 ******************************************************************************/
 

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>
#include <avr/pgmspace.h>
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "aunit.h"
 
 
/******************************************************************************
 * Forward references.
 ******************************************************************************/
 

/******************************************************************************
 * Local definitions.
 ******************************************************************************/


/******************************************************************************
 * Local data.
 ******************************************************************************/
static uint32_t sAssertCount = 0;  //!< Total assertion count
static uint32_t sPassCount = 0;    //!< Total test pass count


/******************************************************************************
 * Public functions.
 ******************************************************************************/
 
/**************************************
 * TEST_INIT
 **************************************/ 
void TEST_INIT(void)
{
    sAssertCount = 0;
    sPassCount = 0;
}


/**************************************
 * TEST_ASSERT_COUNT
 **************************************/ 
uint32_t TEST_ASSERT_COUNT(void)
{
    return sAssertCount;
}


/**************************************
 * TEST_PASS_COUNT
 **************************************/ 
uint32_t TEST_PASS_COUNT(void)
{
    return sPassCount;
}


/**************************************
 * TEST_ASSERT_L
 **************************************/
bool TEST_ASSERT_L(bool cond, uint16_t line)
{
    sAssertCount++;
    
    // Print pass/fail message.
    if (cond)
    {
        Serial.println(F("PASS"));
        sPassCount++;
    }
    else
    {
        Serial.print(F("LINE "));
        Serial.print(line);
        Serial.println(F(" FAIL"));
    }
    return cond;
}


/**************************************
 * TEST_ASSERT_PASS
 **************************************/
bool TEST_ASSERT_PASS(bool cond)
{
    sAssertCount++;
     
    // Print test pass message only.
    if (cond)
    {
        Serial.println(F("PASS"));
        sPassCount++;
    }
    return cond;
}


/**************************************
 * TEST_ASSERT_FAIL_L
 **************************************/
bool TEST_ASSERT_FAIL_L(bool cond, uint16_t line)
{
    sAssertCount++;
     
    // Print fail message only.
    if (!cond)
    {
        Serial.print(F("LINE "));
        Serial.print(line);
        Serial.println(F(" FAIL"));
    }
    else
    {
        sPassCount++;
    }
    return cond;
}


/**************************************
 * TEST_ASSERT_FAIL1_L
 **************************************/
bool TEST_ASSERT_FAIL1_L(bool cond, int32_t x, uint16_t line)
{
    sAssertCount++;
     
    // Print fail message only + loop variables.
    if (!cond)
    {
        Serial.print(F("LINE "));
        Serial.print(line);
        Serial.print(F(" FAIL ("));
        Serial.print(x);
        Serial.println(F(")"));
    }
    else
    {
        sPassCount++;
    }
    return cond;
}


/**************************************
 * TEST_ASSERT_FAIL2_L
 **************************************/
bool TEST_ASSERT_FAIL2_L(bool cond, int32_t x, int32_t y, uint16_t line)
{
    sAssertCount++;
     
    // Print fail message only + loop variables.
    if (!cond)
    {
        Serial.print(F("LINE "));
        Serial.print(line);
        Serial.print(F(" FAIL ("));
        Serial.print(x);
        Serial.print(',');
        Serial.print(y);
        Serial.println(F(")"));
    }
    else
    {
        sPassCount++;
    }
    return cond;
}


/**************************************
 * TEST_WAIT
 **************************************/
void TEST_WAIT()
{
    while (Serial.available()) {Serial.read();}
    Serial.print(F("Press a key to start: "));
    while (!Serial.available()) {}
    Serial.read();
    Serial.println();
}


/**************************************
 * TEST_NUMBER
 **************************************/
void TEST_NUMBER(uint16_t num)
{
    Serial.print(num);
    Serial.print(F(": "));
}


/**************************************
 * TEST_DONE
 **************************************/
void TEST_DONE()
{
    Serial.println(F("TEST DONE"));
}

/******************************************************************************
 * Private functions.
 ******************************************************************************/


/******************************************************************************
 * Interrupt service routines.
 ******************************************************************************/
 

// End of file.
