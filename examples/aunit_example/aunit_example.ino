/******************************************************************************
 * aunit_example.ino
 * Copyright (c) 2015 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief aunit Arduino unit test helper function example code.
 */
 
/******************************************************************************
 * Lint options.
 ******************************************************************************/
 
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "aunit.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD  (115200)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Local data.
 ******************************************************************************/
static uint16_t testNum = 0;


/******************************************************************************
 * Public functions.
 ******************************************************************************/
 
 
/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Initialize serial port and wait for it to connect.
    Serial.begin(SERIAL_BAUD);
    while (!Serial) {}
    Serial.println("aunit example sketch");
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{
    // Wait for user to initiate test.
    TEST_INIT();
    TEST_FILE();
    TEST_WAIT();
    testNum = 0;
    
    TEST_NUMBER(++testNum);
    Serial.println("TEST_ASSERT() pass/fail example");
    TEST_ASSERT(testNum == 1);  // This one will pass
    TEST_ASSERT(testNum == 2);  // This one will fail
    
    TEST_NUMBER(++testNum);
    Serial.println("TEST_ASSERT_FAIL() pass/fail example");
    TEST_ASSERT_FAIL(testNum == 2);  // This one will pass
    TEST_ASSERT_FAIL(testNum == 3);  // This one will fail
    
    TEST_NUMBER(++testNum);
    Serial.println("TEST_ASSERT_FAIL1() pass/fail example");
    TEST_ASSERT_FAIL1(testNum == 3, testNum);  // This one will pass
    TEST_ASSERT_FAIL1(testNum == 4, testNum);  // This one will fail
    
    TEST_NUMBER(++testNum);
    Serial.println("TEST_ASSERT_BREAK() pass/fail example");
    
    // Test will fail when i == 5, and loop will exit
    for (int i = 0; i < 10; i++)
    {
        Serial.print("i = "); Serial.println(i);
        TEST_ASSERT_BREAK(i < 5);
    }
    
    // End of test.  Print stats.
    uint32_t asserts = TEST_ASSERT_COUNT();
    uint32_t passCnt = TEST_PASS_COUNT();
    uint32_t failCnt = asserts - passCnt;
    Serial.print("Total asserts: "); Serial.println(asserts);
    Serial.print("Total pass:    "); Serial.println(passCnt);
    Serial.print("Total fail:    "); Serial.println(failCnt);
    TEST_DONE();
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.